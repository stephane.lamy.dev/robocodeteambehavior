package behavior;

import robocode.HitRobotEvent;
import robocode.MessageEvent;
import robocode.ScannedRobotEvent;
import team.TeamBehavior;

public class SpinCrew implements IBotBehavior {
    @Override
    public void startBot(TeamBehavior bot) {

    }

    @Override
    public void runBot(TeamBehavior bot) {
        bot.setTurnRight(10000.0D);
        bot.setMaxVelocity(5.0D);
        bot.ahead(10000.0D);
    }

    @Override
    public void onScannedBot(TeamBehavior bot, ScannedRobotEvent e) {
        if(!bot.isTeammate(e.getName()) && bot.getTarget().equals("") || bot.getTarget().equals(e.getName()))
            bot.fire(3.0D);
    }

    @Override
    public void onHitBot(TeamBehavior bot, HitRobotEvent e) {
        if (!bot.isTeammate(e.getName()) && e.getBearing() > -10.0D && e.getBearing() < 10.0D) {
            bot.fire(3.0D);
        }

        if (e.isMyFault()) {
            bot.turnRight(10.0D);
        }
    }

    @Override
    public void onMessageReceived(TeamBehavior bot, MessageEvent event) {

    }
}
