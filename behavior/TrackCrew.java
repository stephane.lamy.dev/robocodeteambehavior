package behavior;

import robocode.HitRobotEvent;
import robocode.MessageEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import team.TeamBehavior;

public class TrackCrew implements IBotBehavior {
    int count = 0;
    double gunTurnAmt = 10.0D;

    @Override
    public void startBot(TeamBehavior bot) {

    }

    @Override
    public void runBot(TeamBehavior bot) {
        bot.turnGunRight(this.gunTurnAmt);
        ++this.count;
        if (this.count > 2) {
            this.gunTurnAmt = -10.0D;
        }

        if (this.count > 5) {
            this.gunTurnAmt = 10.0D;
        }
    }

    @Override
    public void onScannedBot(TeamBehavior bot, ScannedRobotEvent e) {
        if (e.getName().equals(bot.getTarget())) {
            this.count = 0;
            if (e.getDistance() > 150.0D) {
                this.gunTurnAmt = Utils.normalRelativeAngleDegrees(e.getBearing() + (bot.getHeading() - bot.getRadarHeading()));
                bot.turnGunRight(this.gunTurnAmt);
                bot.turnRight(e.getBearing());
                bot.ahead(e.getDistance() - 140.0D);
            } else {
                this.gunTurnAmt = Utils.normalRelativeAngleDegrees(e.getBearing() + (bot.getHeading() - bot.getRadarHeading()));
                bot.turnGunRight(this.gunTurnAmt);
                bot.fire(3.0D);
                if (e.getDistance() < 100.0D) {
                    if (e.getBearing() > -90.0D && e.getBearing() <= 90.0D) {
                        bot.back(40.0D);
                    } else {
                        bot.ahead(40.0D);
                    }
                }

                bot.scan();
            }
        }
    }

    @Override
    public void onHitBot(TeamBehavior bot, HitRobotEvent e) {
        //this.back(5.0D);
    }

    @Override
    public void onMessageReceived(TeamBehavior bot, MessageEvent event) {

    }
}
