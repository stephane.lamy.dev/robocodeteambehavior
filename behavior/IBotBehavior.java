package behavior;

import robocode.HitRobotEvent;
import robocode.MessageEvent;
import robocode.ScannedRobotEvent;
import team.TeamBehavior;

public interface IBotBehavior {
    void startBot(TeamBehavior bot);
    void runBot(TeamBehavior bot);
    void onScannedBot(TeamBehavior bot, ScannedRobotEvent e);
    void onHitBot(TeamBehavior bot, HitRobotEvent e);
    void onMessageReceived(TeamBehavior bot, MessageEvent event);
}
