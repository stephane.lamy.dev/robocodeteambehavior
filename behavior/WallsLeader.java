package behavior;

import robocode.HitRobotEvent;
import robocode.MessageEvent;
import robocode.ScannedRobotEvent;
import team.TeamBehavior;

public class WallsLeader implements IBotBehavior {
    boolean peek;
    double moveAmount;

    @Override
    public void startBot(TeamBehavior bot) {
        this.moveAmount = Math.max(bot.getBattleFieldWidth(), bot.getBattleFieldHeight());
        this.peek = false;
        bot.turnLeft(bot.getHeading() % 90.0D);
        bot.ahead(this.moveAmount);
        this.peek = true;
        bot.turnGunRight(90.0D);
        bot.turnRight(90.0D);
    }

    @Override
    public void runBot(TeamBehavior bot) {
        this.peek = true;
        bot.ahead(this.moveAmount);
        this.peek = false;
        bot.turnRight(90.0D);
    }

    @Override
    public void onScannedBot(TeamBehavior bot, ScannedRobotEvent e) {
        if(bot.isTeammate(e.getName()))
            return;

        bot.out.println("potential target found " + e.getName());
        if(bot.setTargetIfNone(e.getName())) {
            bot.out.println("target selected " + e.getName());
            bot.notifyTargetChange();
            //bot.broadcastMessage(new TeamBehavior.AttackHimMessage(e.getName()));
        }

        bot.fire(2.0D);
        if (this.peek) {
            bot.scan();
        }
    }

    @Override
    public void onHitBot(TeamBehavior bot, HitRobotEvent e) {
        if (e.getBearing() > -90.0D && e.getBearing() < 90.0D) {
            bot.back(100.0D);
        } else {
            bot.ahead(100.0D);
        }
    }

    @Override
    public void onMessageReceived(TeamBehavior bot, MessageEvent event) {

    }
}
