package team;

import behavior.SpinCrew;
import behavior.TrackRangeCrew;
import behavior.WallsLeader;

public class SpinWallsTeam extends TeamBehavior {
    public SpinWallsTeam() {
        super(new SpinCrew(), new WallsLeader());
    }
}
