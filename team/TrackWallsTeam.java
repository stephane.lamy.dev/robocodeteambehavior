package team;

import behavior.TrackRangeCrew;
import behavior.WallsLeader;

public final class TrackWallsTeam extends TeamBehavior {
    public TrackWallsTeam() {
        super(new TrackRangeCrew(200D, 80D), new WallsLeader());
    }
}
