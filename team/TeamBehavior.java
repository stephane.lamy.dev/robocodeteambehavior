package team;

import behavior.IBotBehavior;
import behavior.TrackRangeCrew;
import behavior.WallsLeader;
import prog.TeamworkRobot;
import robocode.*;

import java.awt.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TeamBehavior extends TeamRobot implements Serializable {
    // Tank information
    private int id;
    private String target = "";

    // Leader information
    private boolean isLeader = false;
    private String leaderName = "";

    // System for the election system
    private int timer = 0;
    public final static int ACK_TIMER = 10;
    public final static int COORD_TIMER = 40;

    // System for checking if leader is alive
    private int notLeaderTimer = 0;
    private boolean awaitingResponse = false;
    public final static int ASK_LEADER_TIMER = 20;
    public final static int WAIT_LEADER_TIMER = 100;

    // Tank behaviors, either crewmate or leader
    IBotBehavior crewmateBehavior = new TrackRangeCrew(200D, 80D);
    IBotBehavior leaderBehavior = new WallsLeader();

    enum States {
        IDLE,
        WAIT_ACK,
        WAIT_COORD
    }
    States state = States.IDLE;

    public TeamBehavior(IBotBehavior crewmate, IBotBehavior leader) {
        crewmateBehavior = crewmate;
        leaderBehavior = leader;
    }

    @Override
    public void run() {
        this.setBodyColor(Color.lightGray);
        this.setGunColor(Color.magenta);
        this.setRadarColor(Color.yellow);

        id = getID();
        startElection();

        if (isLeader) {
            leaderBehavior.startBot(this);
        } else {
            crewmateBehavior.startBot(this);
        }

        while(true) {
            out.println(state + " : " + timer);
            out.println("target : " + getTarget());

            runElection();

            if (isLeader) {
                leaderBehavior.runBot(this);
            } else {
                crewmateBehavior.runBot(this);
            }
        }
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        runElection();
        if (isLeader) {
            leaderBehavior.onScannedBot(this, e);
        } else {
            crewmateBehavior.onScannedBot(this, e);
        }
    }

    @Override
    public void onHitRobot(HitRobotEvent e) {
        if (isLeader) {
            leaderBehavior.onHitBot(this, e);
        } else {
            crewmateBehavior.onHitBot(this, e);
        }
    }

    @Override
    public void onMessageReceived(MessageEvent event) {
        System.out.println(event.getMessage());
        if(event.getMessage() instanceof ElectionMessage message) {
            if(id > message.getSenderID()) {
                out.println("Election received");
                try {
                    sendMessage(event.getSender(), new AcknowledgementMessage());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if(event.getMessage() instanceof AcknowledgementMessage message) {
            if(state.equals(States.WAIT_ACK)) {
                out.println("Acknowledgement received");
                /*timer = 0;
                state = States.WAIT_COORD;
                setBodyColor(Color.orange);*/
                setState(States.WAIT_COORD);
            }
        }
        if(event.getMessage() instanceof CoordinatorMessage message) {
            out.println("Coordinator received");
            leaderName = event.getSender();
            //target = "";
            //timer = 0;
            notLeaderTimer = 0;
            awaitingResponse = false;
            //state = States.IDLE;
            //this.setBodyColor(Color.lightGray);
            setState(States.IDLE);
        }
        if(event.getMessage() instanceof AttackHimMessage message) {
            //if(event.getSender().equals(leaderName)) {
                out.println("Attack " + message.targetName + "!");
                target = message.targetName;
                scan();
            //}
        }
        if(event.getMessage() instanceof String message) {
            if(message.equals("OK?") && isLeader) {
                out.println("j'ai recu OK?");
                try {
                    sendMessage(event.getSender(), "OK");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(message.equals("OK") && !isLeader) {
                out.println("j'ai recu OK");
                awaitingResponse = false;
                notLeaderTimer = 0;
            }
        }

        if (isLeader) {
            leaderBehavior.onMessageReceived(this, event);
        } else {
            crewmateBehavior.onMessageReceived(this, event);
        }
    }

    @Override
    public void onRobotDeath(RobotDeathEvent event) {
        //out.println("========" + event.getName());
        if(target.equals(event.getName()))
            target = "";
    }

    private int getID() {
        String name = getName();
        Pattern pattern = Pattern.compile("(\\d+)(?!.*\\d)");
        Matcher matcher = pattern.matcher(name);
        if (matcher.find())
        {
            return Integer.parseInt(matcher.group(0));
        }
        return 0;
    }

    public String getTarget() {
        return target;
    }

    public boolean setTargetIfNone(String target) {
        if(this.target.equals("")) {
            this.target = target;
            return true;
        }
        return false;
    }

    private void runElection() {
        if(state != States.IDLE) {
            out.println(id + "reached");
            timer++;

            switch (state) {
                case WAIT_ACK:
                    if(timer > ACK_TIMER) {
                        out.println("ACK timeout");
                        setToLeader();
                        try {
                            broadcastMessage(new CoordinatorMessage());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case WAIT_COORD:
                    if(timer > COORD_TIMER) {
                        out.println("COORD timeout");
                        startElection();
                    }
                    break;
            }
        } else if (!leaderName.isEmpty()) {
            notLeaderTimer++;
            out.println(notLeaderTimer);
            if(awaitingResponse && notLeaderTimer > WAIT_LEADER_TIMER) {
                awaitingResponse = false;
                leaderName = "";
                startElection();
            }
            if(!awaitingResponse && notLeaderTimer > ASK_LEADER_TIMER) {
                try {
                    sendMessage(leaderName, "OK?");
                    awaitingResponse = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setToLeader() {
        isLeader = true;
        /*timer = 0;
        state = States.IDLE;*/
        setState(States.IDLE);
        target = "";
        setBodyColor(Color.red);
        leaderBehavior.startBot(this);
    }

    public void notifyTargetChange() {
        try {
            broadcastMessage(new AttackHimMessage(target));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startElection() {
        try {
            out.println("start election");
            broadcastMessage(new ElectionMessage(id));
            /*timer = 0;
            state = States.WAIT_ACK;
            setBodyColor(Color.green);*/
            setState(States.WAIT_ACK);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setState(States state) {
        this.state = state;
        this.timer = 0;
        switch(state) {
            case IDLE -> setBodyColor(Color.lightGray);
            case WAIT_ACK -> setBodyColor(Color.green);
            case WAIT_COORD -> setBodyColor(Color.yellow);
        }
    }

    public static class ElectionMessage implements Serializable {
        public ElectionMessage(int senderID) {
            this.senderID = senderID;
        }

        public int getSenderID() {
            return senderID;
        }

        int senderID;
    }

    public static class AcknowledgementMessage implements Serializable {
    }

    public static class CoordinatorMessage implements Serializable {
    }

    public static class AttackHimMessage implements Serializable {
        public AttackHimMessage(String targetName) {
            this.targetName = targetName;
        }

        String targetName;
    }
}
