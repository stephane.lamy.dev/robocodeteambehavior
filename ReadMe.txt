Guide d'installation :

1) Rendez-vous sur le site "https://robocode.sourceforge.io/".

2) Dans la section "Robocoding", cliquez sur "Download".

3) Cliquez sur "Download Latest Version". La dernière version actuelle est 1.9.5.0.

4) Une fois le téléchargement terminé, double-cliquez sur le fichier JAR et suivez les instructions d'installation par défaut.

5) Ouvrez votre IDE dédié au développement en Java puis créez un nouveau projet en faisant bien attention de se mettre dans le dossier "robots" qui a été installé à l'emplacement ".../robocode/robots".

6) Dans l'arborescence du projet, supprimez le dossier "src".

7) Sélectionnez tous les éléments du dossier englobant "robots" sauf le fichier nommé "robot.iml" puis cliquez-droit -> Mark Directory as -> Excluded. Les dossiers sélectionnés devraient devenir orange.

8) Cliquez-droit sur le dossier englobant "robots" -> Mark Directory as -> Sources Root.

9) Créez un nouveau package, pour cela, cliquez-droit sur le dossier englobant "robots" -> New -> Package et nommez-le comme vous le souhaitez.

10) Insérez l'ensemble des classes que nous vous avons rendues dans ce package en les copiant-collant à l'intérieur.

11) Dirigez-vous maintenant dans File -> Project Structure... -> Modules -> Paths.

12) Cochez le bouton radio "Use module compile output path" puis remplacez le champ "Output path" par le chemin vers votre dossier englobant "robots" et remplacez également le champ "Test output path" par le même chemin que précédemment mais en pointant sur un sous-dossier du dossier englobant "robots", lui aussi nommé "robots".

13) Décochez la case "Exclude output paths" puis cliquez sur "Apply".

14) Dans le menu à gauche, rendez-vous dans l'onglet "Libraries".

15) Cliquez sur l'icône du "+" en haut à gauche puis sur "Java".

16) Sélectionnez tous les éléments contenus dans le dossier "/robocode/libs/" puis cliquez sur "OK", puis encore "OK".

17) Vous pouvez renommer cette structure via le champ "Name" en haut de la fenêtre puis cliquez sur "Apply", puis "OK".

18) Rendez-vous dans l'onglet "Problems". Au moins deux problèmes devraient apparaître, cherchez celui nommé "Library Robocode ... is not used [Fix]", puis cliquez sur "Fix", puis "OK". Les problèmes devraient disparaître.

19) Dans le menu à gauche, revenez dans "Modules" puis "Dependencies".

20) Sélectionnez "bcel-...", puis cliquez sur l'icône du "-", puis "OK".

21) Avant de lancer le projet, dirigez-vous vers le menu déroulant situé juste à côté du bouton pour l'exécution (icône d'une flèche verte), puis allez dans "Edit Configurations...".

22) Dans la section "Build and run", remplacez le texte du deuxième champ par le texte ci-dessous, puis cliquez sur "OK" :

-Xmx512M
-Djava.security.manager=allow
-XX:+IgnoreUnrecognizedVMOptions
"--add-opens=java.base/sun.net.www.protocol.jar=ALL-UNNAMED"
"--add-opens=java.base/java.lang.reflect=ALL-UNNAMED"
"--add-opens=java.desktop/javax.swing.text=ALL-UNNAMED"
"--add-opens=java.desktop/sun.awt=ALL-UNNAMED"

23) Vous pouvez désormais compiler le projet puis l'exécuter sans erreur.
